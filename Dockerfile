FROM tiangolo/nginx-rtmp

COPY nginx.conf /etc/nginx/nginx.conf
COPY index.html /var/www/
COPY *.png /var/www/
COPY *.jpg /var/www/
RUN mkdir -p /var/www/hls/
RUN chown www-data:www-data /var/www/hls/
RUN mkdir -p /var/www/dash/
RUN chown www-data:www-data /var/www/dash/
